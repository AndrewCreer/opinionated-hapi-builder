# Instructions

```bash

mkdir my-new-api
yarn init


yarn add -D yarn add -D @mrgoose/opinionated-hapi-builder

# Could be a post-install script...

curl https://raw.githubusercontent.com/github/gitignore/main/Node.gitignore >.gitignore

echo "module.exports = {...require('@mrgoose/opinionated-hapi-builder/prettier.config.js')}" >prettier.config.js
yarn prettier -w --print-width 20 prettier.config.js

echo '{"extends":"@mrgoose/opinionated-hapi-builder/tsconfig.json"}' >tsconfig.json
yarn prettier -w --print-width 20 tsconfig.json

# Update the scripts

brew install yq

yq '.scripts."build:interfaces" = "yarn ts-node node_modules/@mrgoose/opinionated-hapi-builder/src/scripts/joi-to-typescript.ts"'  package.json -P -i -o json

yq '.scripts."build:swagger" = "yarn ts-node node_modules/@mrgoose/opinionated-hapi-builder/src/scripts/hapi-swagger.ts"'  package.json -P -i -o json

```
