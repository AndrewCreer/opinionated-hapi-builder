import Joi from 'joi'

const Base1Schema = Joi.object({
  id: Joi.string()
    .guid()
    .required()
    .description('The ID of the base')
    .example(`74422E1F-C418-4EE3-B226-5070E0D11F8C`),
  name: Joi.string()
    .regex(/^[A-Z][[a-z].*$]/)
    .required()
    .example('Goose')
    .description('A name with a leading capital letter')
})

const Base2Schema = Joi.object({
  base2Id: Joi.number()
    .required()
    .min(100)
    .max(999)
    .length(3)
    .example(123)
    .description('A 3 digit integer'),
  hasMeaning: Joi.boolean()
    .default(false)
    .required()
    .example(true)
    .description('to test default vs example')
})

export const ComposedSchema = Joi.object({
  base1: Base1Schema,
  base2Extended: Base2Schema.concat(
    Joi.object({
      extraName: Joi.string()
        .example('George')
        .required()
        .description('An Extra name field just for laughs')
    })
  )
})
