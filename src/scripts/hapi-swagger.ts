/**
 * In which we
 *  - start the server
 *  - register the HapiSwagger plugin
 *  - hit the /documentation endpoint
 *  - save the swagger into a file
 **/

import type {Server} from '@hapi/hapi'
import * as HapiSwagger from 'hapi-swagger'
import * as Inert from '@hapi/inert'
import * as Vision from '@hapi/vision'
import request from 'supertest'
import {writeFileSync, readFileSync} from 'fs'
import {resolve} from 'path'

const pack = JSON.parse(readFileSync(resolve('.', 'package.json')).toString())

const swaggerOptions = {
  info: {title: pack.name}
}

;(async () => {
  // TODO: the location of the file with the start and init could be a cli param
  // although if you move the file you might split the functions into different
  // files.
  const serverFn = resolve('.', 'src/server/server.ts')

  console.log(`Generating swagger: doing\n  const {start, init} = await import(${serverFn})`)
  const {start, init} = await import(serverFn)

  const server: Server = await init()

  await server.register([
    {plugin: Inert},
    {plugin: Vision},
    {
      plugin: HapiSwagger,
      options: swaggerOptions
    }
  ])

  await start(server)
  const response = await request(server.listener).get('/swagger.json')
  await server.stop()

  const filename = resolve('.', 'swagger.json')
  const swagger = JSON.stringify(response.body, null, ' ')
  console.log(`Writing json swagger to file '${filename}'`)
  writeFileSync(filename, swagger)
})()
