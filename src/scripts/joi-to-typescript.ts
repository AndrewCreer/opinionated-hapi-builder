// https://github.com/mrjono1/joi-to-typescript#example-call

import {convertFromDirectory} from 'joi-to-typescript'

convertFromDirectory({
  schemaDirectory: './src/schemas',
  typeOutputDirectory: './src/interfaces',
  debug: true
})
